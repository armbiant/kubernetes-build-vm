package main

import (
	"github.com/google/uuid"
	k8s "k8s.io/api/core/v1"
	"time"
)

type Device struct {
	ResourceName k8s.ResourceName  `yaml:"resourceName"`
	InstanceID   string            `yaml:"instanceID"`
	Env          map[string]string `yaml:"env"`
	UUID         uuid.UUID         `yaml:"-"`
}

type DeviceConfig struct {
	DeviceMapping []struct {
		Devices []Device `yaml:"devices"`
		Hosts   []string `yaml:"hosts"`
	} `yaml:"deviceMapping"`
}

func (d Device) String() string {
	return string(d.ResourceName) + "#" + d.InstanceID
}

type PluginConfig struct {
	hostname   string
	configFile struct {
		path         string
		pollInterval time.Duration
	}
	configMap struct {
		Name string
		Key  string
	}
}
