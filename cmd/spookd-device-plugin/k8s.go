package main

import (
	"io/ioutil"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const NsFile = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"

func getCurrentNamespace() string {
	nsBytes, err := ioutil.ReadFile(NsFile)
	if err != nil {
		panic(err.Error())
	}
	return string(nsBytes)
}

func getKubernetesClientset() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	return clientset
}
