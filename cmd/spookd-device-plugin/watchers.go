package main

import (
	"context"
	"fmt"
	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	k8s "k8s.io/api/core/v1"
	k8sapis "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8swatch "k8s.io/apimachinery/pkg/watch"
	k8sclient "k8s.io/client-go/kubernetes"
	"os"
	"os/signal"
	"time"
)

func newFSWatcher(files ...string) (*fsnotify.Watcher, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	for _, f := range files {
		err = watcher.Add(f)
		if err != nil {
			_ = watcher.Close() // ignore error return, it's actually always nil
			return nil, err
		}
	}

	return watcher, nil
}

func newOSWatcher(sigs ...os.Signal) chan os.Signal {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, sigs...)
	return sigChan
}

type DeviceWatcher struct {
	hostname string
	updates  chan []Device
	stop     chan struct{}
}

func (w *DeviceWatcher) Stop() {
	close(w.stop)
	for range w.updates {
		// drain updates and wait for it to close
	}
}

func newDeviceWatcher(hostname string) *DeviceWatcher {
	return &DeviceWatcher{
		hostname: hostname,
		updates:  make(chan []Device),
		stop:     make(chan struct{}),
	}
}

func NewConfigMapWatcher(cfg PluginConfig) *DeviceWatcher {
	ns := getCurrentNamespace()
	cs := getKubernetesClientset()
	w := newDeviceWatcher(cfg.hostname)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		defer cancel()
		<-w.stop
	}()

	go func() {
		defer close(w.updates)
		for {
			err := watchConfigMap(ctx, w, cs, ns, cfg.configMap.Name, cfg.configMap.Key)
			if err != nil {
				log.Error(err)
				select {
				case <-time.After(5 * time.Second):
					continue
				case <-ctx.Done():
					break
				}
			}
			return
		}
	}()

	return w
}

func watchConfigMap(ctx context.Context, w *DeviceWatcher, client *k8sclient.Clientset, ns string, cmName string, key string) error {
	watcher, err := client.CoreV1().ConfigMaps(ns).Watch(ctx, k8sapis.SingleObject(k8sapis.ObjectMeta{Name: cmName}))
	if err != nil {
		return fmt.Errorf("couldn't create ConfigMap watcher: %w", err)
	}
	defer watcher.Stop()
	log.Debugf("Created watcher for ConfigMap %s in namespace %s", cmName, ns)

	for event := range watcher.ResultChan() {
		log.Infof("ConfigMap change detected with type %s", event.Type)
		switch event.Type {
		case k8swatch.Added:
			fallthrough
		case k8swatch.Modified:
			buf := event.Object.(*k8s.ConfigMap).Data[key]
			w.handleConfigUpdate([]byte(buf))
		default:
			log.Debugf("Unhandled ConfigMap change %s", event)
		}
	}

	// if context has been cancelled, the closure is expected
	if ctx.Err() == nil {
		return fmt.Errorf("ConfigMap watcher connection closed unexpectedly")
	} else {
		return nil
	}
}

func (w *DeviceWatcher) handleConfigUpdate(buf []byte) {
	var deviceConfig DeviceConfig
	err := yaml.UnmarshalStrict(buf, &deviceConfig)
	if err != nil {
		log.Errorf("Failed to parse config as YAML: %s", err)
		return
	}

	devices, err := GenerateDevices(w.hostname, deviceConfig)
	if err != nil {
		log.Errorf("DeviceMapping failed validation: %s", err)
		return
	}
	if devices != nil {
		w.updates <- devices
	}
}

func NewFileWatcher(cfg PluginConfig) *DeviceWatcher {
	w := newDeviceWatcher(cfg.hostname)
	go pollDevices(w, cfg.configFile.path, cfg.configFile.pollInterval)
	return w
}

func pollDevices(w *DeviceWatcher, deviceConfigPath string, interval time.Duration) {
	// TODO: use a filesystem watcher rather than polling

	defer close(w.updates)
	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	// ticker doesn't tick until one interval has passed
	// and doesn't close its channel on Stop(), so hack it...
	trigger := make(chan time.Time)
	go func() {
		defer close(trigger)
		trigger <- time.Now() // emulate an initial tick
		for {
			select {
			case <-w.stop:
				return
			case t := <-ticker.C:
				trigger <- t
			}
		}
	}()

	for range trigger {
		reader, _ := os.Open(deviceConfigPath)
		buf, err := ioutil.ReadAll(reader)
		if err != nil {
			log.Errorf("failed to read config file: %s", err)
		}
		w.handleConfigUpdate(buf)
	}
	log.Debugf("%p: exiting device config poll loop", w)
}
