package main

import (
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ska-telescope/ska-ser-k8s-spookd/pkg/set"
	k8s "k8s.io/api/core/v1"
	"reflect"
	"sync"
)

type DeviceMap map[k8s.ResourceName]map[uuid.UUID]Device

// DevicePluginDirector aggregates servers for all discovered device types.
type DevicePluginDirector struct {
	servers map[k8s.ResourceName]*DevicePluginServer
	devices DeviceMap
	watcher *DeviceWatcher
	done    <-chan struct{}
}

func (pd *DevicePluginDirector) Stop() {
	log.Debugf("%p: stopping device plugin", pd)
	pd.watcher.Stop()
	<-pd.done
}

// NewDevicePluginDirector returns an initialized DevicePluginDirector.
func NewDevicePluginDirector(cfg PluginConfig) *DevicePluginDirector {
	// create local binding as chan rather than <-chan
	done := make(chan struct{})

	var watcher *DeviceWatcher
	if cfg.configFile.path == "" {
		watcher = NewConfigMapWatcher(cfg)
	} else {
		watcher = NewFileWatcher(cfg)
	}

	pd := &DevicePluginDirector{
		watcher: watcher,
		servers: make(map[k8s.ResourceName]*DevicePluginServer),
		done:    done, // is closed after exhausting devWatcher.updates
	}
	log.Debugf("created device plugin director")

	go func() {
		defer close(done)

		var wg sync.WaitGroup

		for devs := range pd.watcher.updates {
			newDevMap := buildDeviceMap(devs)

			created, _ := pd.applyDeviceUpdate(newDevMap)
			// For each created type, wait for it
			for devType := range created {
				wg.Add(1)
				go func(d k8s.ResourceName) {
					err := <-pd.servers[d].done
					if err != nil {
						close(pd.servers[d].updates)
						pd.watcher.updates <- devs
					} else {
						wg.Done()
					}
				}(devType)
			}
		}

		log.Debugf("%p: sending nil update to all plugin servers", pd)
		pd.applyDeviceUpdate(nil)

		log.Debugf("%p: waiting for all plugin servers to stop", pd)
		wg.Wait()

		log.Debugf("%p: all plugin servers stopped, exiting director", pd)
	}()

	log.Debug("started device plugin director goroutine")
	return pd
}

func (pd *DevicePluginDirector) applyDeviceUpdate(newDevMap DeviceMap) (set.Set[k8s.ResourceName], set.Set[k8s.ResourceName]) {
	// work out which device types have been added and removed
	oldTypes := set.FromMap(pd.devices)
	newTypes := set.FromMap(newDevMap)
	deleteTypes := oldTypes.Difference(newTypes)
	createTypes := newTypes.Difference(oldTypes)

	// if a device type hasn't appeared before, create and start its server
	for devType := range createTypes {
		log.Debugf("%p: Creating server for: %s", pd, devType)
		pd.servers[devType] = NewWidgetDevicePluginServer(devType)
		pd.servers[devType].MustServe()
	}

	// pass an updates to the servers for all device types that have changed
	for devType, subMap := range newDevMap {
		if !reflect.DeepEqual(subMap, pd.devices[devType]) {
			log.Debugf("%p: Updating server for: %s", pd, devType)
			pd.servers[devType].updates <- subMap
		}
	}

	// if a known device type doesn't appear, stop and delete its server
	for devType := range deleteTypes {
		log.Debugf("%p: Deleting server for: %s", pd, devType)
		pd.servers[devType].updates <- nil
		close(pd.servers[devType].updates)
		delete(pd.servers, devType)
	}

	pd.devices = newDevMap

	return createTypes, deleteTypes
}

func buildDeviceMap(devices []Device) DeviceMap {
	// map from device type to a map of device ID to Device
	devMap := make(DeviceMap)
	for _, device := range devices {
		if subMap, ok := devMap[device.ResourceName]; ok {
			subMap[device.UUID] = device
		} else {
			subMap = make(map[uuid.UUID]Device)
			devMap[device.ResourceName] = subMap
			subMap[device.UUID] = device
		}
	}
	return devMap
}
