FROM --platform=$BUILDPLATFORM golang:1.18.3 AS builder

WORKDIR /tmp/src

# cache dependencies
COPY go.* ./
RUN go mod download && go mod verify

COPY . ./

ARG TARGETOS
ARG TARGETARCH
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} make build

FROM ubuntu:20.04
COPY --from=builder /tmp/src/bin/* /usr/local/bin/
CMD ["spookd-device-plugin"]
