package set

type Set[T comparable] map[T]struct{}

func (m Set[T]) Add(e T) {
	m[e] = struct{}{}
}

func (m Set[T]) Remove(e T) {
	delete(m, e)
}

func FromMap[T comparable, V any](m map[T]V) Set[T] {
	set := make(Set[T], len(m))
	for k := range m {
		set.Add(k)
	}
	return set
}

func FromSlice[T comparable](s []T) Set[T] {
	set := make(Set[T], len(s))
	for _, k := range s {
		set.Add(k)
	}
	return set
}

func NewSet[T comparable](ks ...T) Set[T] {
	return FromSlice(ks)
}

func (m Set[T]) Contains(e T) bool {
	_, ok := m[e]
	return ok
}

func (m Set[T]) Intersection(n Set[T]) Set[T] {
	intersection := make(Set[T])
	for k := range m {
		if n.Contains(k) {
			intersection.Add(k)
		}
	}
	return intersection
}

func (m Set[T]) Difference(n Set[T]) Set[T] {
	difference := make(Set[T])
	for k := range m {
		if !n.Contains(k) {
			difference.Add(k)
		}
	}
	return difference
}

func (m Set[T]) Union(n Set[T]) Set[T] {
	union := make(Set[T])
	for k := range m {
		union.Add(k)
	}
	for k := range n {
		union.Add(k)
	}
	return union
}

func (m Set[T]) Equals(n Set[T]) bool {
	if len(m) != len(n) {
		return false
	}
	for k := range m {
		if !n.Contains(k) {
			return false
		}
	}
	return true
}
