EXE:=spookd-device-plugin
RESOURCE ?= skao.int~1subrack
NODE ?= minikube

DOCKER ?= docker

.DEFAULT: $(DEVICE)

# MacOS users will be running this in minikube. May also need to set GOARCH.
export GOOS ?= linux

# Support for multi-arch builds. We use docker buildx, which requires
# the push to happen at the same time as the build since docker CLI
# doesn't support manifest lists, so skip the normal push process.
OCI_SKIP_PUSH = 1
# Using override lets us append to the value passed on the command line in
# the pipeline job, and defining this here instead of at the top level avoids
# double-appending when the oci-build-all target itself invokes make
oci-image-build: override OCI_BUILD_ADDITIONAL_ARGS+=--platform=linux/amd64,linux/arm64 --push

# creates a buildx builder with driver 'docker-container' and uses it.
# The --platform arg causes these to be built "locally" rather than in
# a container, which is fine in Golang since the compiler can spit out
# a binary for any platform by setting GOARCH and GOOS.
oci-pre-build-all:
	docker buildx create --use --platform=linux/amd64,linux/arm64 --name $${CI_PROJECT_PATH_SLUG}_$${CI_JOB_ID}
	docker buildx install

# tear down the builder
oci-post-build-all:
	docker buildx rm

-include .make/base.mk
-include .make/oci.mk
-include .make/helm.mk

oci-do-publish:
	docker run --security-opt seccomp=unconfined --rm quay.io/skopeo/stable:latest copy --all \
		--src-creds=$${CI_REGISTRY_USER}:$${CI_REGISTRY_PASSWORD} \
		--dest-creds=$${CAR_OCI_REGISTRY_USERNAME}:$${CAR_OCI_REGISTRY_PASSWORD} \
		docker://$${CI_REGISTRY}/$${CI_PROJECT_NAMESPACE}/$${CI_PROJECT_NAME}/$(OCI_IMAGE):$(VERSION)-dev.c$${CI_COMMIT_SHORT_SHA} \
		docker://$(CAR_OCI_REGISTRY_HOST)/$(OCI_IMAGE):$(VERSION)

-include PrivateRules.mak

build: fmt vet bin/$(EXE)

bin/$(EXE): $(wildcard */*/*.go) go.mod go.sum
	mkdir -p bin
	go build -o bin ./...

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet lint checking against code
vet:
	go vet ./...

clean:
	rm -r bin

docker: build
	$(DOCKER) build -t $(EXE):latest .

run:
	./bin/$(EXE) -h || true

docker_run:
	$(DOCKER) run --rm -ti $(EXE):$(TAG) -h || true

install:
	kubectl apply -f spookd-device-plugin.yml

delete: delete-plugin delete-resources

delete-resources:
	kubectl proxy &
	sleep 2
	curl --header "Content-Type: application/json-patch+json" --request PATCH --data '[{"op": "remove", "path": "/status/allocatable/skao.int~1tpm"},{"op": "remove", "path": "/status/capacity/skao.int~1tpm"}]' http://localhost:8001/api/v1/nodes/$(NODE)/status
	sleep 2
	curl --header "Content-Type: application/json-patch+json" --request PATCH --data '[{"op": "remove", "path": "/status/allocatable/skao.int~1subrack"},{"op": "remove", "path": "/status/capacity/skao.int~1subrack"}]' http://localhost:8001/api/v1/nodes/$(NODE)/status
	pkill -9 -f "kubectl proxy"

delete-plugin:
	kubectl delete --ignore-not-found --cascade=foreground -f spookd-device-plugin.yml

test:
	kubectl apply -f tpm-test-device.yaml

untest:
	kubectl delete -f tpm-test-device.yaml

logs:
	kubectl -n kube-system logs -l app.kubernetes.io/name=spookd-device-plugin

show:
	# spec.containers[].resources.limits.memory
	kubectl run tpm --image=busybox:1.28.3 --overrides='{"apiVersion":"v1","spec":{"containers":[{"name":"tpm","image": "busybox:1.28.3","command":["sh","-c", "sleep 3600"],"resources":{"limits":{"skatelescope.org/tpm":"1"}}}]}}'
	@echo ""; echo ""
	kubectl get pods tpm -o wide
	@echo ""; echo ""
	kubectl wait --for=condition=ready --timeout=60s pod tpm
	@echo ""; echo ""
	kubectl get pods tpm -o wide
	@echo ""; echo ""
	@echo "** pause **"; read X
	@echo ""; echo ""
	kubectl exec -i tpm -- sh -c 'env' | grep SKA
	@echo "** pause **"; read X
	kubectl delete pods/tpm --force
