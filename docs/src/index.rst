Spookd - virtual device plugin for Kubernetes
=============================================

This project implements a Device Plugin for Kubernetes that allows arbitrary extended resources to be advertised and allocated based on a configuration file on the filesystem or in a ConfigMap.

This documentation is generated from the project README.md.

The project's home is on GitLab at https://gitlab.com/ska-telescope/ska-ser-k8s-spookd.

.. toctree::
    readme

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
