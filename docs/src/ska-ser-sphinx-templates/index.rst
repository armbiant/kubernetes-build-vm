.. %REPO_UNDER% documentation master file, created by
   ska-ser-sphinx-templates bootstrap
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to %REPO_UNDER%'s documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
